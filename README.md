# hipages technical exam

This repository is for hipages technical exam. 

# Introduction
This is a protype app for hipages that was created using Kotlin and MVP architectural pattern. It displays a list of job's that was fetched on a server and display it on separate tabs by their status. It utilizes the tablayout, viewpager, recyclerview and cardview to display the content. It also supports different screen resolutions and supports at least api 16 and above. 

# Framework and other technologies
The application was built using the following technology and frameworks:

 - Android SDK
 - Calligraphy
 - CirceImageView
 - Google FlexboxLayout
 - Dagger
 - Retrofit
 - RxJava/RxAndroid
 - CardView