package com.jcl.exam.hipages.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.jcl.exam.hipages.base.BaseActivity
import com.jcl.exam.hipages.data.models.Job
import com.squareup.picasso.Picasso
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by jaylumba on 2019-06-22.
 */
class JobAdapter(private val data: List<Job>, private val activity: BaseActivity, private val picasso: Picasso) : RecyclerView.Adapter<JobAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobAdapter.ViewHolder {
        val view = LayoutInflater.from(activity).inflate(com.jcl.exam.hipages.R.layout.adapter_job, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: JobAdapter.ViewHolder, position: Int) {
        val job = data.get(position)
        holder.tvCategory.text = job.category
        holder.tvPostedDate.text = "Posted: " + getFormattedDate(job.postedDate)
        holder.tvStatus.text = job.status

        if (job.connectedBusinesses != null && job.connectedBusinesses!!.size > 0) {
            val message: StringBuilder = StringBuilder("You have hired ")
                    .append(job.connectedBusinesses!!.size)
            if (job.connectedBusinesses!!.size > 1) message.append(" businesses")
            else message.append(" business")
            holder.tvConnectedDescription.text = message

            val layoutManager = FlexboxLayoutManager(activity)
            layoutManager.flexDirection = FlexDirection.ROW
            layoutManager.flexWrap = FlexWrap.WRAP
            layoutManager.alignItems = AlignItems.FLEX_END

            val adapter = BusinessAdapter(job.connectedBusinesses!!, activity, picasso)
            holder.rvConnectedJobs.layoutManager = layoutManager
            holder.rvConnectedJobs.adapter = adapter
        } else {
            holder.tvConnectedDescription.text = activity.getStringResource(com.jcl.exam.hipages.R.string.connecting_with_businesses)
        }

        holder.ivMenu.setOnClickListener {
            val popup = PopupMenu(activity, it)
            val inflater = popup.menuInflater
            inflater.inflate(com.jcl.exam.hipages.R.menu.job_menu, popup.menu)
            popup.show()
        }

        holder.btnViewDetails.setOnClickListener {
            activity.showToastInfo("Details")
        }
    }


    fun getFormattedDate(dateString: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val cal = Calendar.getInstance()
        var date: Date? = null
        try {
            date = format.parse(dateString)
            cal.time = date
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val day = cal.get(Calendar.DATE)
        if (day !in 11..18)
            return when (day % 10) {
                1 -> SimpleDateFormat("d'st'  MMMM yyyy", Locale.getDefault()).format(date)
                2 -> SimpleDateFormat("d'nd'  MMMM yyyy", Locale.getDefault()).format(date)
                3 -> SimpleDateFormat("d'rd'  MMMM yyyy", Locale.getDefault()).format(date)
                else -> SimpleDateFormat("d'th'  MMMM yyyy", Locale.getDefault()).format(date)
            }
        return SimpleDateFormat("d'th'  MMMM yyyy", Locale.getDefault()).format(date)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivMenu: AppCompatImageView = itemView.findViewById(com.jcl.exam.hipages.R.id.ivMenu)
        val tvCategory: AppCompatTextView = itemView.findViewById(com.jcl.exam.hipages.R.id.tvCategory)
        val tvPostedDate: AppCompatTextView = itemView.findViewById(com.jcl.exam.hipages.R.id.tvPostedDate)
        val tvStatus: AppCompatTextView = itemView.findViewById(com.jcl.exam.hipages.R.id.tvStatus)
        val tvConnectedDescription: AppCompatTextView = itemView.findViewById(com.jcl.exam.hipages.R.id.tvConnectedDescription)
        val rvConnectedJobs: RecyclerView = itemView.findViewById(com.jcl.exam.hipages.R.id.rvConnectedJobs)
        val btnViewDetails: AppCompatButton = itemView.findViewById(com.jcl.exam.hipages.R.id.btnViewDetails)
    }
}