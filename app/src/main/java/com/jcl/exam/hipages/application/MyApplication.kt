package com.jcl.exam.hipages.application

import android.app.Activity
import android.app.Application
import android.app.Service
import androidx.multidex.MultiDexApplication
import com.jcl.exam.hipages.BuildConfig
import com.jcl.exam.hipages.R
import com.jcl.exam.hipages.di.DaggerAppComponent
import com.jcl.exam.hipages.encryption.AesCbcWithIntegrity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.security.GeneralSecurityException
import javax.inject.Inject

/**
 * Created by jaylumba on 05/16/2018.
 */
class MyApplication : MultiDexApplication(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    override fun onCreate() {
        super.onCreate()

        /** initialize timber  */
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        /** initialize calligraphy  */
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())


        /** initialize dagger */
        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this)

        /** initialize encryption key */
        try {
            keys = AesCbcWithIntegrity.generateKeyFromPassword(BuildConfig.SECRET_KEY, BuildConfig.SECRET_KEY)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }

    override fun serviceInjector(): AndroidInjector<Service> {
        return dispatchingServiceInjector
    }

    companion object {

        var keys: AesCbcWithIntegrity.SecretKeys? = null
            private set
    }

}