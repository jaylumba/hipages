package com.jcl.exam.hipages.exceptions

open class BaseException(cause: Throwable) : RuntimeException(cause)
