package com.jcl.exam.hipages.data.api

import com.jcl.exam.hipages.data.api.responses.JobsResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.Response
import retrofit2.http.GET
import retrofit2.http.Header

/**
 * Created by jaylumba on 05/16/2018.
 */

interface ApiInterface {
    @GET("/hipgrp-assets/tech-test/jobs.json")
    fun getJobs(): Observable<JobsResponse>
}
