package com.exist.phr.di

import com.jcl.exam.hipages.ui.jobs.di.JobsFragmentProvider
import com.jcl.exam.hipages.ui.main.MainActivity
import com.jcl.exam.hipages.ui.main.di.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by jaylumba on 11/18/2017.
 */

@Module
abstract class BuildersModule{

    @ContributesAndroidInjector(modules = [MainModule::class, JobsFragmentProvider::class])
    internal abstract fun bindMainActivity(): MainActivity

}
