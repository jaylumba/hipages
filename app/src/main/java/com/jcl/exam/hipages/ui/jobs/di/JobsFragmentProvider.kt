package com.jcl.exam.hipages.ui.jobs.di


import com.jcl.exam.hipages.ui.jobs.JobsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class JobsFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(JobsModule::class))
    internal abstract fun provideJobsFragmentFactory(): JobsFragment
}
