package com.jcl.exam.hipages.data.api.responses

import com.jcl.exam.hipages.data.models.Job

/**
 * Created by jaylumba on 2019-06-22.
 */
data class JobsResponse(var jobs: ArrayList<Job>)