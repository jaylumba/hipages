package com.jcl.exam.hipages.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.jcl.exam.hipages.ui.jobs.JobsFragment

import java.util.ArrayList

class BaseFragmentPagerAdapter(fm: FragmentManager, private val fragments: ArrayList<Fragment>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val fragment = fragments[position] as JobsFragment
        return fragment.getTitle()
    }

    override fun getCount(): Int {
        return fragments.size
    }
}