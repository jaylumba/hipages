package com.jcl.exam.hipages.ui.jobs.di

import com.jcl.exam.hipages.ui.jobs.JobsFragment
import com.jcl.exam.hipages.ui.jobs.mvp.JobsContract
import com.jcl.exam.hipages.ui.jobs.mvp.JobsPresenter
import com.jcl.exam.hipages.util.scheduler.AppSchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class JobsModule {

    @Provides
    internal fun provideView(fragment: JobsFragment): JobsContract.View {
        return fragment
    }

    @Provides
    internal fun providePresenter(appSchedulerProvider: AppSchedulerProvider, view: JobsContract.View): JobsPresenter {
        return JobsPresenter(appSchedulerProvider, view)
    }
}
