package com.jcl.exam.hipages.ui.main.mvp;

import com.jcl.exam.hipages.base.BaseView
import com.jcl.exam.hipages.data.api.responses.JobsResponse


interface MainContract {
    interface View : BaseView {
        fun setupToolbarAndDrawer()
        fun setupViewPager(jobsResponse: JobsResponse)
    }

    interface Presenter {
        fun onLoad()
        fun getJobs()
    }
}