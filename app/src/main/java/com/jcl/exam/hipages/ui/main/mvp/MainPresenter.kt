package com.jcl.exam.hipages.ui.main.mvp;

import com.jcl.exam.hipages.data.api.JobService
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jcl.exam.hipages.base.BasePresenter
import com.jcl.exam.hipages.ui.main.MainActivity
import com.jcl.exam.hipages.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import timber.log.Timber


class MainPresenter(private val schedulerProvider: SchedulerProvider, view: MainContract.View, private val jobService: JobService) : BasePresenter<MainContract.View>(view), MainContract.Presenter {

    private val TAG = MainActivity::class.java.simpleName
    private val requestStateObserver = BehaviorRelay.createDefault(RequestState.IDLE)

    init {
        observeRequestState()
    }

    private fun publishRequestState(requestState: RequestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(schedulerProvider.ui())
                .subscribe(requestStateObserver))
    }

    private fun observeRequestState() {
        addDisposable(requestStateObserver.subscribe({ requestState ->
            when (requestState) {
                RequestState.IDLE -> {
                }
                RequestState.LOADING -> view.setLoadingIndicator(true)
                RequestState.COMPLETE -> view.setLoadingIndicator(false)
                RequestState.ERROR -> view.setLoadingIndicator(false)
                else -> {
                }
            }
        }, { Timber.e(it) }))
    }

    override fun onLoad() {
        view.setupToolbarAndDrawer()
        getJobs()
    }

    override fun getJobs() {
        if (view.isNetworkAvailable()) {
            addDisposable(jobService.getJobs()
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { view.setLoadingIndicator(true) }
                    .subscribe({ jobsResponse ->
                        view.setLoadingIndicator(false)
                        view.setupViewPager(jobsResponse)
                    }, { throwable ->
                        throwable.printStackTrace()
                        view.setLoadingIndicator(false)
                    }))
        } else {
            view.showToastError("Please check internet connection and try again!")
        }
    }
}