package com.jcl.exam.hipages.interfaces

/**
 * Created by jlumba on 11/16/17.
 */

interface OnRetryListener {
    fun retry()
}
