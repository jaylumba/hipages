package com.jcl.exam.hipages.ui.main.di;

import com.jcl.exam.hipages.data.api.JobService
import com.jcl.exam.hipages.ui.main.MainActivity
import com.jcl.exam.hipages.ui.main.mvp.MainContract
import com.jcl.exam.hipages.ui.main.mvp.MainPresenter
import com.jcl.exam.hipages.util.scheduler.AppSchedulerProvider
import dagger.Module;
import dagger.Provides;


@Module
class MainModule {

    @Provides
    internal fun provideView(activity: MainActivity): MainContract.View {
        return activity
    }

    @Provides
    internal fun providePresenter(appSchedulerProvider: AppSchedulerProvider, view: MainContract.View, service: JobService): MainPresenter {
        return MainPresenter(appSchedulerProvider, view, service)
    }
}