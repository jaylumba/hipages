package com.jcl.exam.hipages.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

public class JSONUtils {
    private static String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create();

    public static <T> T fromJson(String json, Class<T> classOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat)
                .create();
        return gson.fromJson(json, classOfT);
    }

    public static String toJson(Object object) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat)
                .registerTypeAdapterFactory(new TypeAdapterFactory() {
                    @Override
                    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                        return null;
                    }
                })
                .create();
        return gson.toJson(object);
    }
}
