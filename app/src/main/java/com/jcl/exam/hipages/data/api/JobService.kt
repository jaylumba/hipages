package com.jcl.exam.hipages.data.api

import com.jcl.exam.hipages.data.api.responses.JobsResponse
import io.reactivex.Observable

/**
 * Created by jaylumba on 05/16/2018.
 */
class JobService (private var apiInterface: ApiInterface){
    fun getJobs(): Observable<JobsResponse> {
        return apiInterface.getJobs()
    }
}