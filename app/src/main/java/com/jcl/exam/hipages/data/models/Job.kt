package com.jcl.exam.hipages.data.models

/**
 * Created by jaylumba on 2019-06-22.
 */
data class Job(var jobId: Int) {

    var category: String = ""
    var postedDate: String = ""
    var status: String = ""
    var connectedBusinesses: ArrayList<Business>? = null
    var detailsLink: String = ""

    data class Business(var businessId: Int) {
        var thumbnail: String = ""
        var isHired: Boolean = false
    }

}