package com.jcl.exam.hipages.ui.jobs.mvp

import com.jcl.exam.hipages.base.BaseView

interface JobsContract {
    interface View : BaseView {
        fun initRecyclerview()
    }

    interface Presenter {
        fun onLoad()
    }
}
