package com.jcl.exam.hipages.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.jcl.exam.hipages.base.BaseActivity
import com.jcl.exam.hipages.data.models.Job
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import android.view.Gravity
import android.widget.GridLayout
import android.widget.RelativeLayout
import com.google.android.flexbox.FlexboxLayoutManager


/**
 * Created by jaylumba on 2019-06-22.
 */
class BusinessAdapter(private val data: List<Job.Business>, private val activity: BaseActivity, private val picasso: Picasso) : RecyclerView.Adapter<BusinessAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessAdapter.ViewHolder {
        val view = LayoutInflater.from(activity).inflate(com.jcl.exam.hipages.R.layout.adapter_business, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: BusinessAdapter.ViewHolder, position: Int) {
        val business = data[position]

        picasso.load(business.thumbnail).into(holder.ivThumbnail)

        if (business.isHired) holder.tvHiredStatus.visibility = View.VISIBLE
        else holder.tvHiredStatus.visibility = View.GONE

        val lp2 = holder.rlContainer.layoutParams
        if (lp2 is FlexboxLayoutManager.LayoutParams) {
            lp2.flexGrow = 1f
            lp2.flexBasisPercent = 0.25f
        }

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlContainer: RelativeLayout = itemView.findViewById(com.jcl.exam.hipages.R.id.rlContainer)
        val ivThumbnail: CircleImageView = itemView.findViewById(com.jcl.exam.hipages.R.id.ivThumbnail)
        val tvHiredStatus: AppCompatTextView = itemView.findViewById(com.jcl.exam.hipages.R.id.tvHiredStatus)
    }
}