package com.jcl.exam.hipages.ui.jobs;

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jcl.exam.hipages.R
import com.jcl.exam.hipages.adapters.JobAdapter
import com.jcl.exam.hipages.base.BaseFragment
import com.jcl.exam.hipages.data.models.Job
import com.jcl.exam.hipages.ui.jobs.mvp.JobsContract
import com.jcl.exam.hipages.ui.jobs.mvp.JobsPresenter
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class JobsFragment : BaseFragment(), JobsContract.View {

    private val TAG = JobsFragment::class.java.simpleName

    @Inject
    lateinit var presenter: JobsPresenter

    @Inject
    lateinit var picasso: Picasso

    private var title: String? = null
    private var jobs: List<Job>? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onLoad()
    }

    fun getTitle(): String? {
        return title
    }

    override fun initRecyclerview() {
        val rvJobs = view!!.findViewById<RecyclerView>(R.id.rvJobs)
        rvJobs!!.layoutManager = LinearLayoutManager(baseActivity)
//        val currentStatus = if (title == getStringResource(R.string.tab_openjobs)) "In progress"
//        else "Closed"
//        val jobs = MockDataHelper.getJobsResponse()
//                .jobs.filter { job -> job.status.toUpperCase() == currentStatus.toUpperCase() }
        rvJobs.adapter = JobAdapter(jobs!!, baseActivity, picasso)
    }

    companion object {
        @JvmStatic
        fun newInstance(title: String, jobs: List<Job>): JobsFragment {
            val fragment = JobsFragment()
            fragment.title = title
            fragment.jobs = jobs
            return fragment
        }
    }
}
