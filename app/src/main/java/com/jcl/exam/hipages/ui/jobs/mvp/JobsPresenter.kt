package com.jcl.exam.hipages.ui.jobs.mvp

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jcl.exam.hipages.base.BasePresenter
import com.jcl.exam.hipages.ui.jobs.JobsFragment
import com.jcl.exam.hipages.util.scheduler.SchedulerProvider
import io.reactivex.Observable
import timber.log.Timber

class JobsPresenter(private val schedulerProvider: SchedulerProvider, view: JobsContract.View) : BasePresenter<JobsContract.View>(view), JobsContract.Presenter {

    private val TAG = JobsFragment::class.java.simpleName
    private val requestStateObserver = BehaviorRelay.createDefault(RequestState.IDLE)

    init {
        observeRequestState()
    }

    private fun publishRequestState(requestState: RequestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(schedulerProvider.ui())
                .subscribe(requestStateObserver))
    }

    private fun observeRequestState() {
        addDisposable(requestStateObserver.subscribe({ requestState ->
            when (requestState) {
                RequestState.IDLE -> {
                }
                RequestState.LOADING -> view.setLoadingIndicator(true)
                RequestState.COMPLETE -> view.setLoadingIndicator(false)
                RequestState.ERROR -> view.setLoadingIndicator(false)
                else -> {
                }
            }
        }, { Timber.e(it) }))
    }

    override fun onLoad() {
        view.initRecyclerview()
    }
}
