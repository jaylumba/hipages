package com.jcl.exam.hipages.ui.main

import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.jcl.exam.hipages.R
import com.jcl.exam.hipages.adapters.BaseFragmentPagerAdapter
import com.jcl.exam.hipages.base.BaseActivity
import com.jcl.exam.hipages.data.api.responses.JobsResponse
import com.jcl.exam.hipages.ui.jobs.JobsFragment
import com.jcl.exam.hipages.ui.main.mvp.MainContract
import com.jcl.exam.hipages.ui.main.mvp.MainPresenter
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_app_bar.*
import java.util.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, HasSupportFragmentInjector, NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var toggle: ActionBarDrawerToggle

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onLoad()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_item_one -> Toast.makeText(this, "Info", Toast.LENGTH_SHORT).show()
        }
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setupToolbarAndDrawer() {
        setSupportActionBar(toolbar)
        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun setupViewPager(jobsResponse: JobsResponse) {
        val fragments = ArrayList<Fragment>()
        val jobsInprogress = jobsResponse .jobs.filter { job ->
            job.status.toUpperCase() == "In progress".toUpperCase() }
        val jobsClosed = jobsResponse .jobs.filter { job ->
            job.status.toUpperCase() == "Closed".toUpperCase() }
        fragments.add(JobsFragment.newInstance(getStringResource(R.string.tab_openjobs), jobsInprogress))
        fragments.add(JobsFragment.newInstance(getStringResource(R.string.tab_closedjobs), jobsClosed))
        val adapter = BaseFragmentPagerAdapter(supportFragmentManager, fragments)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = fragments.size
    }
}
